const grid = document.querySelector('.grid-container');

const clearBtn = document.getElementById('clear');

// all items related to menu
const menuBtn = document.getElementById('menu-btn');
const menu = document.getElementById('menu');
const penBtn = document.getElementById('pen');
const eraseBtn = document.getElementById('eraser');
const colorInput = document.getElementsByName('color')[0];

const changeGridBtn = document.getElementById('change-grid');

// changing default behaviour
// removes dragging elemets by accident
grid.addEventListener('dragstart', (e) => {
	e.preventDefault()
});
grid.style.backgroundColor = '#fff'

let color = '#000';

function clearScreen() {
	for (let i = 0; i < grid.children.length; i ++) {
		grid.children[i].style.backgroundColor = '#fff';
	}
}

function selectColor() {
	color =  colorInput.value;
}

function selectPen() {
	let isPenDown
	grid.addEventListener('mousedown', (e) => {
		e.target.style.backgroundColor = color;
		isPenDown = true;
	})
	grid.addEventListener('mouseup', () => {
		isPenDown = false;
	})
	grid.addEventListener('mouseover', (e) => {
		if (isPenDown) {
			e.target.style.backgroundColor = color;
		}
	});
}

function selectEraser() {
	let isPenDown
	grid.addEventListener('mousedown', (e) => {
		e.target.style.backgroundColor = '#fff';
		isPenDown = true;
	})
	grid.addEventListener('mouseup', () => {
		isPenDown = false;
	})
	grid.addEventListener('mouseover', (e) => {
		if (isPenDown) {
			e.target.style.backgroundColor = '#fff';
		}
	});
}

function deletePixels() {
	Array.from(grid.children).forEach((pixel) => {
		pixel.remove();
	});
}

function changeGrid() {
	let numberOfPixels;
	numberOfPixels = Number(prompt('Give X number of pixels (X by X)'));
	while (numberOfPixels > 100) {
		numberOfPixels = Number(prompt('Give X number of pixels (X by X)'));
	}

	deletePixels()

	let i = 0;
	while (i < numberOfPixels * numberOfPixels) {
		const pixel = document.createElement('div');
		pixel.classList.add('pixel');
		pixel.style.backgroundColor = '#fff';
		pixel.style.width = 720 / numberOfPixels + 'px';
		pixel.style.height = 720 / numberOfPixels + 'px';
		grid.appendChild(pixel);
		i++;
	}
}

clearBtn.addEventListener('click', clearScreen);
eraseBtn.addEventListener('click', selectEraser);
changeGridBtn.addEventListener('click', changeGrid);
menuBtn.onclick = () => {
	menu.classList.toggle('show-menu');
}
colorInput.addEventListener('change', selectColor);
penBtn.addEventListener('click', selectPen);
