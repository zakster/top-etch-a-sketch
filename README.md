# Project: Etch-a-Sketch

- [x] Change grid size
- [x] Pen
- [x] Color selection
- [x] Eraser
- [ ] Rainbow pen
- [ ] Shading 
